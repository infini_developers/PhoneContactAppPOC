package com.example.dell.myphoneapp.permissionHandling;

import android.support.v4.app.Fragment;

/**
 * Created by Poona on 5/12/2017.
 */

public class PermissionFragment extends Fragment implements PermissionCallback {
    @Override
    public void onRequestPermissionGranted(String[] permission) {

    }

    @Override
    public void onRequestPermissionDenied(String[] permission) {

    }
}

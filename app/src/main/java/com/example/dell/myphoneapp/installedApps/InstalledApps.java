package com.example.dell.myphoneapp.installedApps;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.util.Log;


import com.example.dell.myphoneapp.utility.Constants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Created by Poona on 7/19/2017.
 */

public class InstalledApps {

    private String package_name;
    private String app_name;
    private String version_name;
    private int version_code;
    static SimpleDateFormat sdf_date_format = new SimpleDateFormat(Constants.DF_yyyy_MM_dd_HH_mm_ss_SSS_Z);

    public static @NonNull
    List<ApplicationInfo> getAllInstalledApps(@NonNull Context context) {
        PackageManager packageManager = context.getPackageManager();
        List<ApplicationInfo> installedApps = packageManager.
                getInstalledApplications(PackageManager.GET_META_DATA);
        if (installedApps == null) {
            installedApps = new ArrayList<>();
        }
        return installedApps;
    }

    public static @NonNull
    String getAppName(@NonNull ApplicationInfo appInfo, @NonNull Context context) {
        PackageManager packageManager = context.getPackageManager();
        return packageManager.getApplicationLabel(appInfo).toString();
    }

    public static @NonNull
    String getAppName(@NonNull String packageName, @NonNull Context context) {
        PackageManager packageManager = context.getPackageManager();
        String appName = "(unknown)";
        try {
            ApplicationInfo appInfo = packageManager.getApplicationInfo(packageName, 0);
            if (appInfo != null) {
                appName = getAppName(appInfo, context);
            }
        } catch (final PackageManager.NameNotFoundException ignored) {
        }
        return appName;
    }

    @SuppressLint("StaticFieldLeak")
    public static List<InstalledApps> getInstalledApps(final Context context) {
        PackageManager packageManager = context.getPackageManager();
        /*List<ApplicationInfo> apps = packageManager.getInstalledApplications(PackageManager.GET_META_DATA);

        List<InstalledApps> arrayList_installedApps = new ArrayList<>();
        for (ApplicationInfo app : apps) {

            if ((app.flags & ApplicationInfo.FLAG_UPDATED_SYSTEM_APP) != 0) {
                //installed_apps.add(app);
            } else if ((app.flags & ApplicationInfo.FLAG_SYSTEM) != 0) {

            } else {
               // Log.e("IA","..."+packageManager.getApplicationLabel(app));
                InstalledApps installedApps = new InstalledApps();
                installedApps.package_name = app.packageName;
                installedApps.app_name=packageManager.getApplicationLabel(app).toString();
                arrayList_installedApps.add(installedApps);
            }
        }*/



        List<PackageInfo> arraylist_PackageInfos = packageManager.getInstalledPackages(0);

        List<InstalledApps> arrayList_installedApps = new ArrayList<>();
        for (int i = 0; i < arraylist_PackageInfos.size(); i++) {
            PackageInfo packageInfo = arraylist_PackageInfos.get(i);
            if ((packageInfo.applicationInfo.flags & ApplicationInfo.FLAG_UPDATED_SYSTEM_APP) != 0) {
                //installed_apps.add(app);
            } else if ((packageInfo.applicationInfo.flags & ApplicationInfo.FLAG_SYSTEM) != 0) {

            } else {
                Log.e("package info.. " + i, packageInfo.packageName);
                Log.e("package info..vname " + i, packageInfo.versionName);
                Log.e("package info..vcode " + i, ""+packageInfo.versionCode);
                Log.e("package info..installtime " + i, ""+packageInfo.firstInstallTime);

                Calendar calendar=Calendar.getInstance();
                calendar.setTimeInMillis(packageInfo.firstInstallTime);
                Log.e("package info..installtimedate " + i, ""+ sdf_date_format.format(calendar.getTime()));
                Log.e("package info..installlocation " + i, ""+packageInfo.installLocation);
                if(packageInfo.installLocation==0)
                {Log.e("package info..installlocation " + i, "auto ");}
                else if(packageInfo.installLocation==1)
                {Log.e("package info..installlocation " + i, "internalOnly ");}
                else
                { Log.e("package info..installlocation " + i, "preferExternal ");}


                InstalledApps installedApps = new InstalledApps();
                installedApps.package_name = packageInfo.packageName;
                installedApps.app_name = packageManager.getApplicationLabel(packageInfo.applicationInfo).toString();
                installedApps.version_name=packageInfo.versionName;
                installedApps.version_code=packageInfo.versionCode;
                arrayList_installedApps.add(installedApps);
            }

        }


        return arrayList_installedApps;
    }

    public String getPackage_name() {
        return package_name;
    }

    public String getApp_name() {
        return app_name;
    }

    public String getVersion_name() {
        return version_name;
    }

    public int getVersion_code() {
        return version_code;
    }

    private void getAppCategories(Context context) throws IOException, JSONException {

        BufferedReader bufferedReader = null;
        HttpURLConnection urlConnection = null;
        BufferedWriter bufferedWriter = null;

        StringBuilder result = new StringBuilder();

        //Create JSON object to send to webservice
        JSONObject jsonObjectSend = new JSONObject();
        JSONArray jsonArrayPakages = new JSONArray();
        PackageManager packageManager;
        List<ResolveInfo> listApps; //this list store all app in device

        try {
            packageManager = context.getPackageManager();
            Intent filterApp = new Intent(Intent.ACTION_MAIN);
            filterApp.addCategory(Intent.CATEGORY_LAUNCHER);
            listApps = packageManager.queryIntentActivities(filterApp,
                    PackageManager.GET_META_DATA);

            for (ResolveInfo app : listApps) {
                jsonArrayPakages.put(app.activityInfo.packageName.trim());
            }

            jsonObjectSend.put("packages", jsonArrayPakages);

            Log.d("json", jsonObjectSend.toString());

            URL url = new URL("http://getdatafor.appspot.com/data?key=53972606b926d38191a5446fdff89e377873d767fabedf6d");
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setConnectTimeout(10000); /* milliseconds */
            urlConnection.setReadTimeout(10000); /* milliseconds */
            urlConnection.setRequestMethod("POST");
            urlConnection.setRequestProperty("Content-Type", "application-json");
            urlConnection.setDoOutput(true); /* allow output to send data */
            urlConnection.connect();

            OutputStream outputStream = urlConnection.getOutputStream();
            bufferedWriter =  new BufferedWriter(new OutputStreamWriter(outputStream));
            bufferedWriter.write(jsonObjectSend.toString());
            bufferedWriter.flush();

            InputStream inputStream = urlConnection.getInputStream();
            bufferedReader = new BufferedReader(new InputStreamReader(inputStream));

            //Read data
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                result.append(line);
            }

            /*Parse JSON**********************************************************************************/
            JSONObject jsonObjectResult = new JSONObject(result.toString().trim());
            JSONArray jsonArrayApps = jsonObjectResult.getJSONArray("apps");

            for (int j = 0; j < jsonArrayApps.length(); j++) {

                JSONObject jsonObjectApp = jsonArrayApps.getJSONObject(j);

                String packageName = jsonObjectApp.getString("package").trim();
                String cate = jsonObjectApp.getString("category").trim();

                Log.d("result", (j + 1) + "---> : " + packageName + "---" + cate);
                Log.d("result", jsonObjectApp.toString());
            }
            /***********************************************************************************/

        } finally {
            if (bufferedReader != null) {
                bufferedReader.close();
            }
            if (bufferedWriter != null) {
                bufferedWriter.close();
            }
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
        }
    }
}

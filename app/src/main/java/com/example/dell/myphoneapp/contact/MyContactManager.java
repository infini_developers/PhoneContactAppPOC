package com.example.dell.myphoneapp.contact;

import android.app.Activity;
import android.app.LoaderManager;
import android.content.CursorLoader;
import android.content.Loader;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.util.Log;


import java.util.ArrayList;

/**
 * Created by Poonam on 01-07-2017.
 */

public class MyContactManager implements LoaderManager.LoaderCallbacks<Cursor> {
    private Activity activity;
    public static final int CONTACT_LOADER = 1;
    public static final int CONTACT_PHONE_LOADER = 2;
    public static final int CONTACT_EMAIL_LOADER=3;
    private OnFinishedLoadingContactData callback;

    Loader<Cursor> loader_contact = null;
    Loader<Cursor> loader_phone = null;
    Bundle bundle = new Bundle();

    public MyContactManager(Activity activity, OnFinishedLoadingContactData callback) {
        this.activity = activity;
        this.callback = callback;

        loader_contact=activity.getLoaderManager().getLoader(CONTACT_LOADER);

        if (loader_contact == null) {
            activity.getLoaderManager().initLoader(CONTACT_LOADER, null, this);
        } else {
            activity.getLoaderManager().restartLoader(CONTACT_LOADER, bundle, this);
        }
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle bundle) {
        Log.e("contacts","onCreateLoader");

        switch (id) {
            case CONTACT_LOADER:
                return new CursorLoader(activity, ContactsContract.Contacts.CONTENT_URI, null, null, null, null);
            case CONTACT_PHONE_LOADER:
                return new CursorLoader(activity, ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, null, null, null);
            case CONTACT_EMAIL_LOADER:
                return new CursorLoader(activity, ContactsContract.CommonDataKinds.Email.CONTENT_URI, null, null, null, null);
        }
        return null;
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {

        switch (loader.getId()) {
            case CONTACT_LOADER:
                MyContact.setCursor(CONTACT_LOADER, data);
                activity.getLoaderManager().restartLoader(CONTACT_PHONE_LOADER, bundle, this);
                break;
            case CONTACT_PHONE_LOADER:
                MyContact.setCursor(CONTACT_PHONE_LOADER, data);
                activity.getLoaderManager().restartLoader(CONTACT_EMAIL_LOADER, bundle, this);
                break;
            case CONTACT_EMAIL_LOADER:
                MyContact.setCursor(CONTACT_EMAIL_LOADER, data);
                ArrayList<MyContact> contactsdata = MyContact.getContactsFrom();
                callback.contactData(contactsdata);
                break;
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }

}

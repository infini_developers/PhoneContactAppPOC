package com.example.dell.myphoneapp.contact;

import android.database.Cursor;
import android.provider.ContactsContract;
import android.util.Log;


import java.util.ArrayList;
import java.util.Date;

/**
 * Created by Poonam on 01-07-2017.
 */

public class MyContact {

    private String contactId;
    private String contactName;
    private Date contactLastUpdatedDate;

    private String phoneContactId;
    private String phoneNumber;

    private String emailContactId;
    private String emailId;

    private ArrayList<MyContact> arrayListContact;
    private ArrayList<MyContact> arrayListEmail;

    static Cursor cursor_contact;
    static Cursor cursor_phone;
    static Cursor cursor_email;

    public static void setCursor(int id, Cursor cursor) {
        switch (id) {
            case MyContactManager.CONTACT_LOADER:
                cursor_contact = cursor;
                break;
            case MyContactManager.CONTACT_PHONE_LOADER:
                cursor_phone = cursor;
                break;
            case MyContactManager.CONTACT_EMAIL_LOADER:
                cursor_email = cursor;
                break;
        }
    }

    public static ArrayList<MyContact> getContactsFrom() {
        int contactIdColumn = cursor_contact.getColumnIndex(ContactsContract.Contacts._ID);
        int contactNameColumn = cursor_contact.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME);
        int contactLastUpdatedDate = cursor_contact.getColumnIndex(ContactsContract.Contacts.CONTACT_LAST_UPDATED_TIMESTAMP);
        int contactHasNumberColumn = cursor_contact.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER);

        int phoneContactIdColumn = cursor_phone.getColumnIndex(ContactsContract.CommonDataKinds.Phone.CONTACT_ID);
        int phoneNumberColumn = cursor_phone.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);

        int emailContactIdColumn = cursor_email.getColumnIndex(ContactsContract.CommonDataKinds.Email.CONTACT_ID);
        int emailIdColumn = cursor_email.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA);

        ArrayList<MyContact> contacts = new ArrayList<>();

        cursor_contact.moveToFirst();
        while (cursor_contact.isAfterLast() == false) {
            MyContact myContact = new MyContact();
            myContact.contactId = cursor_contact.getString(contactIdColumn);
            myContact.contactName = cursor_contact.getString(contactNameColumn);
            String lastUpdatedTimestamp = cursor_contact.getString(contactLastUpdatedDate);
            if (lastUpdatedTimestamp != null) {
                myContact.contactLastUpdatedDate = new Date(Long.parseLong(lastUpdatedTimestamp));
            }
            boolean hasNumber = Boolean.parseBoolean(cursor_contact.getString(contactHasNumberColumn));

            // if(hasNumber)
            //{
            ArrayList<MyContact> arrayList_phone = new ArrayList<>();
            cursor_phone.moveToFirst();
            while (cursor_phone.isAfterLast() == false) {
                String contact_id = cursor_phone.getString(phoneContactIdColumn);
                if (myContact.contactId.equalsIgnoreCase(contact_id)) {
                    MyContact myContactPhone = new MyContact();
                    myContactPhone.phoneContactId = contact_id;
                    Log.e("myContactPhone.phoneContactId",myContactPhone.phoneContactId);
                    myContactPhone.phoneNumber = cursor_phone.getString(phoneNumberColumn);
                    arrayList_phone.add(myContactPhone);

                }

                cursor_phone.moveToNext();
            }
            //cursor_phone.close();
            ArrayList<MyContact> arrayList_email = new ArrayList<>();
            cursor_email.moveToFirst();
            while (cursor_email.isAfterLast() == false) {
                String contact_id = cursor_email.getString(emailContactIdColumn);
                if (myContact.contactId.equalsIgnoreCase(contact_id)) {
                    MyContact myContactEmail = new MyContact();
                    myContactEmail.emailId = cursor_email.getString(emailIdColumn);
                    arrayList_email.add(myContactEmail);
                }
                cursor_email.moveToNext();
            }
            //cursor_email.close();
            myContact.arrayListContact = arrayList_phone;
            myContact.arrayListEmail = arrayList_email;
            // }
            contacts.add(myContact);
            cursor_contact.moveToNext();
        }

        cursor_email.close();
        cursor_phone.close();
        cursor_contact.close();

        return contacts;
    }

    public String getContactId() {
        return contactId;
    }

    public String getContactName() {
        return contactName;
    }

    public String getPhoneContactId() {
        return phoneContactId;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public ArrayList<MyContact> getArrayListContact() {
        return arrayListContact;
    }

    public String getEmailContactId() {
        return emailContactId;
    }

    public String getEmailId() {
        return emailId;
    }

    public Date getContactLastUpdatedDate() {
        return contactLastUpdatedDate;
    }

    public ArrayList<MyContact> getArrayListEmail() {
        return arrayListEmail;
    }
}

package com.example.dell.myphoneapp.utility;

public class Constants {
    public static String DF_dd_MM_yyyy = "dd-MM-yyyy";
    public static final String INSTALLED_APPS_PERMISSION="installed apps permission";
    public static String DF_yyyy_MM_dd_HH_mm_ss_SSS_Z = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
}

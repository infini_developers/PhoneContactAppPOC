package com.example.dell.myphoneapp.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Build;
import android.provider.ContactsContract;
import android.support.annotation.RequiresApi;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.telephony.SubscriptionInfo;
import android.telephony.SubscriptionManager;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.Toast;

import com.example.dell.myphoneapp.R;
import com.example.dell.myphoneapp.accounts.Accounts;
import com.example.dell.myphoneapp.installedApps.InstalledApps;
import com.example.dell.myphoneapp.permissionHandling.PermissionActivity;
import com.example.dell.myphoneapp.permissionHandling.RegisterPermission;
import com.example.dell.myphoneapp.utility.Constants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

@RegisterPermission(permissions = {
        Manifest.permission.INTERNET,
        Manifest.permission.READ_CONTACTS,
        Manifest.permission.GET_ACCOUNTS,
        Manifest.permission.READ_CALL_LOG,
        Manifest.permission.WRITE_CALL_LOG,
        Manifest.permission.READ_PHONE_STATE,
        Manifest.permission.READ_SMS,
        Manifest.permission.GET_ACCOUNTS,
        Manifest.permission.PACKAGE_USAGE_STATS

})

public class MainActivity extends PermissionActivity {
    RecyclerView rv_slotlist;
    JSONObject jsonObject_contact;
    JSONObject jsonObject_account;
    JSONObject jsonObject_callLogs;
    JSONObject jsonObject_calendar;
    JSONObject jsonObject_device_info;
    JSONObject jsonObject_installed_apps;
    DateFormat df_dateFormat = new SimpleDateFormat("yyyy-dd-MM HH:mm:ss");
    SimpleDateFormat sdf_date_format = new SimpleDateFormat(Constants.DF_dd_MM_yyyy);

    String device_id;
    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        init();
        getInstalledApps(Constants.INSTALLED_APPS_PERMISSION);

        getContactData(Manifest.permission.READ_CONTACTS);
        getAccounts(Manifest.permission.GET_ACCOUNTS);
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void init() {
        TelephonyManager telephonyManager = ((TelephonyManager) getSystemService(TELEPHONY_SERVICE));
        telephonyManager.getPhoneCount();
        Toast.makeText(MainActivity.this, "" + telephonyManager.getPhoneCount(), Toast.LENGTH_SHORT).show();
        TelephonyInfo telephonyInfo = TelephonyInfo.getInstance(this);

        String imeiSIM1 = telephonyInfo.getImsiSIM1();
        String imeiSIM2 = telephonyInfo.getImsiSIM2();
        String phoneSIM1 = telephonyInfo.getPhoneNumberSIM1();
        String phoneSIM2 = telephonyInfo.getPhoneNumberSIM2();
        String phoneSIM1networkoperatername = telephonyInfo.getPhoneNumberSIM1NetworkOperaterName();
        String phoneSIM2networkoperatername = telephonyInfo.getPhoneNumberSIM2NetworkOperaterName();
        boolean isSIM1Ready = telephonyInfo.isSIM1Ready();
        boolean isSIM2Ready = telephonyInfo.isSIM2Ready();

        boolean isDualSIM = telephonyInfo.isDualSIM();
        Toast.makeText(MainActivity.this, "number1:" + phoneSIM1, Toast.LENGTH_SHORT).show();
        Toast.makeText(MainActivity.this, "number2:" + phoneSIM2, Toast.LENGTH_SHORT).show();
        Toast.makeText(MainActivity.this, "number1type:" + phoneSIM1networkoperatername, Toast.LENGTH_SHORT).show();
        Toast.makeText(MainActivity.this, "number2type:" + phoneSIM2networkoperatername, Toast.LENGTH_SHORT).show();



        List<SubscriptionInfo> subscriptionInfos = SubscriptionManager.from(getApplicationContext()).getActiveSubscriptionInfoList();
        for(int i=0; i<subscriptionInfos.size();i++)
        {
            SubscriptionInfo lsuSubscriptionInfo = subscriptionInfos.get(i);
            Toast.makeText(MainActivity.this, "" + lsuSubscriptionInfo.getNumber(), Toast.LENGTH_SHORT).show();
            Toast.makeText(MainActivity.this, "" + lsuSubscriptionInfo.getCarrierName(), Toast.LENGTH_SHORT).show();
            Toast.makeText(MainActivity.this, "" + lsuSubscriptionInfo.getCountryIso(), Toast.LENGTH_SHORT).show();


        }

    }


    public void getInstalledApps(String permission) {
        List<InstalledApps> arraylist_installed_apps = InstalledApps.getInstalledApps(this);


        try {
            jsonObject_installed_apps = new JSONObject();
            JSONArray jsonArray_installed_apps = new JSONArray();
            for (int i = 0; i < arraylist_installed_apps.size(); i++) {
                String package_name = arraylist_installed_apps.get(i).getPackage_name();
                String app_name = arraylist_installed_apps.get(i).getApp_name();

                Log.e("Package Name", package_name);

                if (!package_name.equals("com.ojassoft.astrosage")) {
                    JSONObject jsonObject_data = new JSONObject();
                    jsonObject_data.put("package_name", package_name);
                    jsonObject_data.put("app_name", app_name);
                    jsonArray_installed_apps.put(jsonObject_data);
                }

            }
         //   jsonObject_installed_apps.put("deviceID", device_id);
            jsonObject_installed_apps.put("installedApps", jsonArray_installed_apps);
            Log.e("InstalledApps JSon...", jsonObject_installed_apps.toString());

        } catch (JSONException ignored) {
        }
    }


    // Contact Details
    @SuppressLint("StaticFieldLeak")
    public void getContactData(final String permission) {

        new AsyncTask<Void, Void, JSONObject>() {
            @Override
            protected void onPreExecute() {

            }// End of onPreExecute method

            @Override
            protected JSONObject doInBackground(Void... params) {
                jsonObject_contact=getContactsV2(permission);
                return jsonObject_contact;
            }// End of doInBackground method

            @Override
            protected void onPostExecute(JSONObject result) {
                Log.e("test",result.toString());

               // sendJsonData(Constants.CONTACTS, jsonObject_contact, permission);
            }//End of onPostExecute method
        }.execute((Void[]) null);
    }

    JSONObject getContactsV2(String permission) {


        try {
            ContentResolver cr = getContentResolver();
            Cursor cur = cr.query(ContactsContract.Contacts.CONTENT_URI, null, null, null, null);
            if (cur.getCount() > 0) {
                jsonObject_contact = new JSONObject();
                JSONArray jsonArray_contact = new JSONArray();
                while (cur.moveToNext()) {
                    String id = cur.getString(cur.getColumnIndex(ContactsContract.Contacts._ID));
                    String contactName = cur.getString(cur.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                    //  String contactName1 = cur.getString(cur.getColumnIndex(ContactsContract.Contacts.CONTENT_ITEM_TYPE));
                    Date contactDt = null;
                    String timestamp = cur.getString(cur.getColumnIndex(ContactsContract.Contacts.CONTACT_LAST_UPDATED_TIMESTAMP));
                    if (timestamp != null) {
                        contactDt = new Date(Long.parseLong(timestamp));
                    }
                    String contactLastUpdatedDate = contactDt != null ? df_dateFormat.format(contactDt) : "";
                    JSONArray jsonArray_phone = new JSONArray();
                    JSONArray jsonArray_phone_type = new JSONArray();
                    JSONArray jsonArray_number_type = new JSONArray();
                    JSONArray jsonArray_email = new JSONArray();
                    if (Integer.parseInt(cur.getString(cur.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))) > 0) {
                        String[] projection = new String[]{
                                ContactsContract.CommonDataKinds.Phone.CONTACT_ID,
                                ContactsContract.CommonDataKinds.Phone.NUMBER,
                                ContactsContract.Data.MIMETYPE,
                                "account_type",
                                ContactsContract.Data.DATA2
                        };

                        String where = ContactsContract.Data.CONTACT_ID + "=?"
                                + " AND " + ContactsContract.Data.MIMETYPE + "=?";

                        // Add contactId filter.
                        String[] selectionArgs = new String[]{
                                String.valueOf(id),
                                ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE
                        };

                        String sortOrder = null;
                        Cursor pCur = cr.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                                projection,
                                where,
                                selectionArgs,
                                sortOrder);
//                        Cursor pCur = cr.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
//                                new String[]{ContactsContract.CommonDataKinds.Phone.CONTACT_ID,
//                                        ContactsContract.CommonDataKinds.Phone.NUMBER,
//                                        ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME,ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE},
//                                ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?",
//                                new String[]{id},
//                                null);
                        while (pCur.moveToNext()) {
                            String phoneNumber = pCur.getString(pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                            //  this.displayContactNumber.add(phoneNumber);
                            phoneNumber = phoneNumber.replaceAll(" ", "");
                            jsonArray_phone.put(phoneNumber);
                            String contactNtype = filterPhone(phoneNumber);
                            jsonArray_number_type.put(contactNtype);
                            String contactName1 = pCur.getString(pCur.getColumnIndex(ContactsContract.Data.DATA2));
                            jsonArray_phone_type.put(contactName1);
                        }
                        Cursor pCur2 = cr.query(ContactsContract.CommonDataKinds.Email.CONTENT_URI,
                                null,
                                ContactsContract.CommonDataKinds.Email.CONTACT_ID + " = ?",
                                new String[]{id},
                                null);

                        while (pCur2.moveToNext()) {
                            String emailId = pCur2.getString(pCur2.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA));
                            //  this.displayContactNumber.add(phoneNumber);
                            jsonArray_email.put(emailId);

                        }

                        pCur.close();
                        pCur2.close();
                        //      pCur3.close();

                    }

                    JSONObject jsonObject_contact = new JSONObject();
                    //jsonObject_contact.put("id", device_id);
                    jsonObject_contact.put("name", contactName);
                    jsonObject_contact.put("phone", jsonArray_phone);
                    jsonObject_contact.put("phonenumbertype", jsonArray_number_type);
                    jsonObject_contact.put("email", jsonArray_email);
                    jsonObject_contact.put("createdAt", contactLastUpdatedDate);
                    jsonObject_contact.put("updatedAt", contactLastUpdatedDate);
                    jsonObject_contact.put("type", jsonArray_phone_type);
                    jsonArray_contact.put(jsonObject_contact);


                }
//        try {
//            jsonObject_contact = new JSONObject();
//            JSONArray jsonArray_contact = new JSONArray();
//                ContentResolver resolver = getContentResolver();
//                Cursor cursor = resolver.query(
//                        ContactsContract.Data.CONTENT_URI,
//                        null, null, null,
//                        ContactsContract.Contacts.DISPLAY_NAME);
//
////Now read data from cursor like
//                String mimeType1="";
//                while (cursor.moveToNext()) {
//                    String _id = cursor.getString(cursor.getColumnIndex(ContactsContract.Data.CONTACT_ID));
//                    String displayName = cursor.getString(cursor.getColumnIndex(ContactsContract.Data.DISPLAY_NAME));
//                    String mimeType = cursor.getString(cursor.getColumnIndex(ContactsContract.Data.MIMETYPE));
//                    mimeType1 = cursor.getString(cursor.getColumnIndex(ContactsContract.Data.DATA2));
//                  //  jsonArray_phone_type.put(mimeType);
//                    Log.d("DataCONTENT_TYPE", _id+ " "+ cursor.getColumnIndex(ContactsContract.Data.HAS_PHONE_NUMBER) + " " + mimeType1 );
//
//                    JSONArray jsonArray_phone = new JSONArray();
//                    JSONArray jsonArray_email = new JSONArray();
//                    if (cursor.getColumnIndex(ContactsContract.Data.HAS_PHONE_NUMBER) > 0) {
//                        Cursor pCur = resolver.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
//                                null,
//                                ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?",
//                                new String[]{_id},
//                                null);
//                        while (pCur.moveToNext()) {
//                            String phoneNumber = pCur.getString(pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
//                            //  this.displayContactNumber.add(phoneNumber);
//                            phoneNumber = phoneNumber.replaceAll(" ", "");
//                            jsonArray_phone.put(phoneNumber);
//
//                        }
//                        Cursor pCur2 = resolver.query(ContactsContract.CommonDataKinds.Email.CONTENT_URI,
//                                null,
//                                ContactsContract.CommonDataKinds.Email.CONTACT_ID + " = ?",
//                                new String[]{_id},
//                                null);
//
//                        while (pCur2.moveToNext()) {
//                            String emailId = pCur2.getString(pCur2.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA));
//                            //  this.displayContactNumber.add(phoneNumber);
//                            jsonArray_email.put(emailId);
//
//                        }
//                        pCur.close();
//                        pCur2.close();
//
//                    }else {
//                        Log.d("Nodfgdfgdf", "No" );
//                    }
//
//
//                    JSONObject jsonObject_contact = new JSONObject();
//                    //jsonObject_contact.put("id", device_id);
//
//                        jsonObject_contact.put("name", displayName);
//                        jsonObject_contact.put("phone", jsonArray_phone);
//                   // jsonObject_contact.put("phonenumbertype", jsonArray_number_type);
//                    jsonObject_contact.put("email", jsonArray_email);
////                    jsonObject_contact.put("createdAt", contactLastUpdatedDate);
////                    jsonObject_contact.put("updatedAt", contactLastUpdatedDate);
//                        jsonObject_contact.put("type", mimeType1);
//                        jsonArray_contact.put(jsonObject_contact);
//



            //  jsonObject_contact.put("deviceID", device_id);
            jsonObject_contact.put("contacts", jsonArray_contact);
            Log.e("Contact JSon.......", "" + jsonObject_contact.getJSONArray("contacts").length());
            Log.e("Contact JSon.......", jsonObject_contact.toString());
        }
                } catch (JSONException e) {
            e.printStackTrace();
        }
       //     }

//        } catch (JSONException e) {
//            Log.e("COntact exception", e.getMessage());
//        }

        return jsonObject_contact;
    }


    public static String filterPhone(String phone_text) {
        String correct = "";

        if ((phone_text.length() <= 12) && (phone_text.matches("^[0-9-]+$")))
            correct = "phone";
        else if(phone_text.matches("^\\+((?:9[679]|8[035789]|6[789]|5[90]|42|3[578]|2[1-689])|9[0-58]|8[1246]|6[0-6]|5[1-8]|4[013-9]|3[0-469]|2[70]|7|1)(?:\\W*\\d){0,13}\\d$"))
            correct ="International";
        else
            correct ="Landline";
        System.out.println("correct =" + correct);
        return correct;

            // InputFilter lengthFilter = new InputFilter.LengthFilter(12);
    }


    public void getAccounts(String permission) {
        ArrayList<Accounts> arrayList_accounts = Accounts.getAccounts(MainActivity.this);

        try {
            jsonObject_account = new JSONObject();
            JSONArray jsonArray_account = new JSONArray();
            for (int i = 0; i < arrayList_accounts.size(); i++) {
                String accountName = arrayList_accounts.get(i).getAccount_name();
                String accountType = arrayList_accounts.get(i).getAccount_type();

                JSONObject jsonObject_data = new JSONObject();
                jsonObject_data.put("name", accountName);
                jsonObject_data.put("type", accountType);
                jsonArray_account.put(jsonObject_data);
            }

            Log.e("Account JSon213", jsonObject_account.toString());
            jsonObject_account.put("deviceID", device_id);
            jsonObject_account.put("identity", jsonArray_account);


        } catch (JSONException ignored) {
            Log.e("Account JSon", ignored.toString());
        }
    }

    private void getAppCategories(Context context) throws IOException, JSONException {

        BufferedReader bufferedReader = null;
        HttpURLConnection urlConnection = null;
        BufferedWriter bufferedWriter = null;

        StringBuilder result = new StringBuilder();

        //Create JSON object to send to webservice
        JSONObject jsonObjectSend = new JSONObject();
        JSONArray jsonArrayPakages = new JSONArray();
        PackageManager packageManager;
        List<ResolveInfo> listApps; //this list store all app in device

        try {
            packageManager = context.getPackageManager();
            Intent filterApp = new Intent(Intent.ACTION_MAIN);
            filterApp.addCategory(Intent.CATEGORY_LAUNCHER);
            listApps = packageManager.queryIntentActivities(filterApp,
                    PackageManager.GET_META_DATA);

            for (ResolveInfo app : listApps) {
                jsonArrayPakages.put(app.activityInfo.packageName.trim());
            }

            jsonObjectSend.put("packages", jsonArrayPakages);

            Log.d("json", jsonObjectSend.toString());

            URL url = new URL("http://getdatafor.appspot.com/data?key=53972606b926d38191a5446fdff89e377873d767fabedf6d");
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setConnectTimeout(10000); /* milliseconds */
            urlConnection.setReadTimeout(10000); /* milliseconds */
            urlConnection.setRequestMethod("POST");
            urlConnection.setRequestProperty("Content-Type", "application-json");
            urlConnection.setDoOutput(true); /* allow output to send data */
            urlConnection.connect();

            OutputStream outputStream = urlConnection.getOutputStream();
            bufferedWriter =  new BufferedWriter(new OutputStreamWriter(outputStream));
            bufferedWriter.write(jsonObjectSend.toString());
            bufferedWriter.flush();

            InputStream inputStream = urlConnection.getInputStream();
            bufferedReader = new BufferedReader(new InputStreamReader(inputStream));

            //Read data
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                result.append(line);
            }

            /*Parse JSON**********************************************************************************/
            JSONObject jsonObjectResult = new JSONObject(result.toString().trim());
            JSONArray jsonArrayApps = jsonObjectResult.getJSONArray("apps");

            for (int j = 0; j < jsonArrayApps.length(); j++) {

                JSONObject jsonObjectApp = jsonArrayApps.getJSONObject(j);

                String packageName = jsonObjectApp.getString("package").trim();
                String cate = jsonObjectApp.getString("category").trim();

                Log.d("result", (j + 1) + "---> : " + packageName + "---" + cate);
                Log.d("result", jsonObjectApp.toString());
            }
            /***********************************************************************************/

        } finally {
            if (bufferedReader != null) {
                bufferedReader.close();
            }
            if (bufferedWriter != null) {
                bufferedWriter.close();
            }
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
        }
    }
}

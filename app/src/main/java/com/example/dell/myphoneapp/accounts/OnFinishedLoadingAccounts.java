package com.example.dell.myphoneapp.accounts;

import java.util.ArrayList;

/**
 * Created by Poona on 6/27/2017.
 */

public interface OnFinishedLoadingAccounts {
    void account(ArrayList<Accounts> accounts);
}

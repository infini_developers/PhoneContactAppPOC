package com.example.dell.myphoneapp.accounts;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.Context;

import java.util.ArrayList;

/**
 * Created by Poona on 6/27/2017.
 */

public class Accounts {

    private String account_name;
    private String account_type;

    public static ArrayList<Accounts> getAccounts(Context context)
    {
        Account[] array_accounts = AccountManager.get(context).getAccounts();

        ArrayList<Accounts> arrayList_accounts=new ArrayList<>();

        for(Account account:array_accounts)
        {
            Accounts accounts=new Accounts();
            accounts.account_name=account.name;
            accounts.account_type=account.type;

            arrayList_accounts.add(accounts);
        }
        return arrayList_accounts;
    }

    public String getAccount_name() {
        return account_name;
    }

    public String getAccount_type() {
        return account_type;
    }
}

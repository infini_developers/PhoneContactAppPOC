package com.example.dell.myphoneapp.contact;

import java.util.ArrayList;

/**
 * Created by Poonam on 01-07-2017.
 */

public interface OnFinishedLoadingContactData {
    void contactData(ArrayList<MyContact> contactdata);
}
